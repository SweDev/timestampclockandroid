package com.example.pparadis.timestampclock;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    Context context;
    Button startWorkButton;
    Button startLunchButton;
    Button stopLunchButton;
    Button stopWorkButton;
    Button clearTimesButton;
    Button saveTimesButton;
    TextView startWorkTextView;
    TextView startLunchTextView;
    TextView stopLunchTextView;
    TextView stopWorkTextView;
    int duration = Toast.LENGTH_SHORT;
    Calendar timeOfStartOfWork;
    Calendar timeOfEndOfWork;
    Calendar timeOfStartOfLunch;
    Calendar timeOfEndOfLunch;
    SimpleDateFormat simpleDateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String startWorkDateTime;
    String stopWorkDateTime;
    String startLunchDateTime;
    String stopLunchDateTime;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;

    // (7 * 60 * 60000) + (30 * 60000) 7,5 hrs
    public enum RequiredWorkingTimes{
        //MONDAY(27000000),TUESDAY(27000000),WEDNESDAY(27000000),THURSDAY(27000000),FRIDAY(25200000);
        MONDAY(2),TUESDAY(3),WEDNESDAY(4),THURSDAY(5),FRIDAY(6);

        private final long value;

        RequiredWorkingTimes(final long value){
            this.value = value;
        }
        public long getValue() {return value;};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        context = getApplicationContext();

        startWorkButton = (Button) findViewById(R.id.start_work_button);
        startLunchButton = (Button) findViewById(R.id.start_lunch_button);
        stopLunchButton = (Button) findViewById(R.id.stop_lunch_button);
        stopWorkButton = (Button) findViewById(R.id.stop_work_button);
        clearTimesButton = (Button) findViewById(R.id.clear_times_button);
        saveTimesButton = (Button) findViewById(R.id.save_times_button);

        startWorkTextView = (TextView) findViewById(R.id.textWorkStart);
        startLunchTextView = (TextView) findViewById(R.id.textLunchStart);
        stopLunchTextView = (TextView) findViewById(R.id.textLunchStop);
        stopWorkTextView = (TextView) findViewById(R.id.textWorkStop);

        dbHelper = new DatabaseHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();
    }

    public void startWork(View view) {
        timeOfStartOfWork = Calendar.getInstance();
        startWorkDateTime = simpleDateFormater.format(timeOfStartOfWork.getTime());

        db.execSQL("INSERT INTO " + "TIMES" + "(STARTWORK) "
                   + "VALUES(" + timeOfStartOfWork.getTimeInMillis() + ")");
        CharSequence text = "Work Started";
        Toast toast = Toast.makeText(context, text+" at "+startWorkDateTime, duration);
        toast.show();
        startWorkTextView.setText(text+" at "+startWorkDateTime);
    }

    public void startLunch(View view) {
        timeOfStartOfLunch = Calendar.getInstance();
        startLunchDateTime = simpleDateFormater.format(timeOfStartOfLunch.getTime());

        db.execSQL("UPDATE TIMES SET STARTLUNCH=" + timeOfStartOfLunch.getTimeInMillis()
                   + " WHERE ID=(SELECT MAX(ID) FROM TIMES)");

        CharSequence text = "Lunch Started";
        Toast toast = Toast.makeText(context, text+" at "+startLunchDateTime, duration);
        toast.show();
        startLunchTextView.setText(text+" at "+startLunchDateTime);
    }

    public void stopLunch(View view) {
        timeOfEndOfLunch = Calendar.getInstance();
        stopLunchDateTime = simpleDateFormater.format(timeOfEndOfLunch.getTime());

        db.execSQL("UPDATE TIMES SET STOPLUNCH=" + timeOfEndOfLunch.getTimeInMillis()
                   + " WHERE ID=(SELECT MAX(ID) FROM TIMES)");

        CharSequence text = "Lunch Ended";
        Toast toast = Toast.makeText(context, text+" at "+stopLunchDateTime, duration);
        toast.show();
        stopLunchTextView.setText(text+" at "+stopLunchDateTime);
    }

    public void stopWork(View view) {
        EnumMap<RequiredWorkingTimes, Integer> workingTimes = new EnumMap<RequiredWorkingTimes, Integer>(RequiredWorkingTimes.class);
        workingTimes.put(RequiredWorkingTimes.MONDAY, 27000000);
        workingTimes.put(RequiredWorkingTimes.TUESDAY, 27000000);
        workingTimes.put(RequiredWorkingTimes.WEDNESDAY, 27000000);
        workingTimes.put(RequiredWorkingTimes.THURSDAY, 27000000);
        workingTimes.put(RequiredWorkingTimes.FRIDAY, 25200000);

        timeOfEndOfWork = Calendar.getInstance();
        stopWorkDateTime = simpleDateFormater.format(timeOfEndOfWork.getTime());

        db.execSQL("UPDATE TIMES SET STOPWORK=" + timeOfEndOfWork.getTimeInMillis()
                   + " WHERE ID=(SELECT MAX(ID) FROM TIMES)");

        long workedTimeMSToday = (timeOfEndOfWork.getTimeInMillis() - timeOfStartOfWork.getTimeInMillis())
                                 - (timeOfEndOfLunch.getTimeInMillis() - timeOfStartOfLunch.getTimeInMillis());

        int dayOfWeek = timeOfEndOfWork.get(Calendar.DAY_OF_WEEK);
        long requiredWorkTimeMSToday = 0L;
        Iterator<RequiredWorkingTimes> keySet = workingTimes.keySet().iterator();
        while(keySet.hasNext()){
            RequiredWorkingTimes requiredWorkingTimeMSToday = keySet.next();
            if(requiredWorkingTimeMSToday.getValue() == dayOfWeek){
                requiredWorkTimeMSToday = workingTimes.get(requiredWorkingTimeMSToday);
            }
        }
        //long requiredWorkTimeMSToday = (7 * 60 * 60000) + (30 * 60000);

        long flexTimeMSToday =  workedTimeMSToday - requiredWorkTimeMSToday;
        long flexTimeMinToday = (long) ((flexTimeMSToday / 1000) / 60);

        db.execSQL("UPDATE TIMES SET FLEXTIME=" + flexTimeMSToday
                + " WHERE ID=(SELECT MAX(ID) FROM TIMES)");

        CharSequence text = "Work Ended";
        Toast toast = Toast.makeText(context, text+" at "+stopWorkDateTime  + "/nFlextime amount today: " + flexTimeMinToday, duration);
        toast.show();
        stopWorkTextView.setText(text+" at "+stopWorkDateTime);
    }

    public void clearTimes(View view) {

        Cursor cursor = db.query("TIMES",new String[]{"ID","STARTWORK"},null,null,null,null,null);

        if(cursor != null) {
            cursor.moveToFirst();
            try {
                do{
                    long unixDateTime = cursor.getLong(1);
                    Calendar dateTime = Calendar.getInstance();
                    dateTime.setTimeInMillis(unixDateTime);
                    if(dateTime.get(Calendar.YEAR) == timeOfStartOfWork.get(Calendar.YEAR)
                       && dateTime.get(Calendar.MONTH) == timeOfStartOfWork.get(Calendar.MONTH)
                       && dateTime.get(Calendar.DAY_OF_MONTH) == timeOfStartOfWork.get(Calendar.DAY_OF_MONTH)){
                        int idOfRow = cursor.getInt(0);
                        db.execSQL("DELETE FROM TIMES "
                                   + "WHERE ID=" + idOfRow);
                    }
                }while(cursor.moveToNext());
            } finally {
                cursor.close();
            }
        }

        timeOfStartOfWork = null;
        startWorkDateTime = "";
        startWorkTextView.setText("Time of work start display here");

        timeOfEndOfWork = null;
        stopWorkDateTime = "";
        stopWorkTextView.setText("Time of work stop display here");

        timeOfStartOfLunch = null;
        startLunchDateTime = "";
        startLunchTextView.setText("Time of lunch start display here");

        timeOfEndOfLunch = null;
        stopLunchDateTime = "";
        stopLunchTextView.setText("Time of lunch stop display here");


        CharSequence text = "All times have been cleared!";
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public void saveTimes(View view) {

    }
}
