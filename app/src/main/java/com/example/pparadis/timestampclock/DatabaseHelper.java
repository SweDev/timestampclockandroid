package com.example.pparadis.timestampclock;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by pparadis on 18-11-2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "timestampclock.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "times";
    private static final String TAG = "DatabaseHelper";

    DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS times"); // Remove this line after testing
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
        + "ID " + "INTEGER PRIMARY KEY " + "AUTOINCREMENT " + ","
        + "STARTWORK "  + "INTEGER" + ","
        + "STARTLUNCH " + "INTEGER" + ","
        + "STOPLUNCH "  + "INTEGER" + ","
        + "STOPWORK "   + "INTEGER" + ","
        + "FLEXTIME "   + "INTEGER"
        + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            Log.w(TAG, "Upgrading database from version " + oldVersion + "to "
            + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS times");
        onCreate(db);
    }
}
